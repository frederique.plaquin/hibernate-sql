package entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@NamedQuery(name = "Employee.byDept", query = "SELECT e FROM Employee e WHERE e.dept.name= ?1")
public class Employee { // has to be same name as table name
    private long id;
    private String firstName;
    private String lastName;
    private Department dept;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "firstname")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "lastname")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (id != employee.id) return false;
        if (!Objects.equals(firstName, employee.firstName)) return false; // if (firstName != null ? !firstName.equals(employee.firstName) : employee.firstName != null) return false;
        return Objects.equals(lastName, employee.lastName); // return lastName != null ? lastName.equals(employee.lastName) : employee.lastName == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "department_id", referencedColumnName = "id")
    public Department getDept() {
        return dept;
    }

    public void setDept(Department departmentByDepartmentId) {
        this.dept = departmentByDepartmentId;
    }

    // show mysql results as string in terminal
    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstname='" + firstName + '\'' +
                ", lastname='" + lastName + '\'' +
                ", dept=" + dept +
                '}';
    }
}