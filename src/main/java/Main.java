import entity.Employee;

import javax.persistence.*;

public class Main {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();

        try {
            transaction.begin();

            TypedQuery<Employee> empByDeptQuery = entityManager.createNamedQuery("Employee.byDept", Employee.class);

            // set query method parameters
            empByDeptQuery.setParameter(1, "Java Advocacy");

            // print results
            System.out.println("------------------------------------------------------------------------------------");
            System.out.println("--------------------------------NAMED QUERY RESULT----------------------------------");
            for(Employee employee: empByDeptQuery.getResultList()) {
                System.out.println(employee);
            }


            // EXECUTING SQL QUERY
            Query countEmpByDept =  entityManager.createNativeQuery("SELECT COUNT(*) FROM entreprise.employee " +
                    "INNER JOIN department d on employee.department_id = d.id WHERE d.name=:deptName");
            countEmpByDept.setParameter("deptName", "Java Advocacy");

            // PRINTING SQL QUERY RESULT
            System.out.println("------------------------------------------------------------------------------------");
            System.out.println("---------------------------------SQL QUERY RESULT-----------------------------------");
            System.out.println("There are " + countEmpByDept.getSingleResult() + " Java Advocates.");

            transaction.commit();

        } finally {
            if(transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
    }
}
